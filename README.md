# Livre de cuisine

## English

This repo is only in french ;) Sorry.

## Edito

Ce projet sert juste à stocker mes recettes de cuisines, ne pas passer 3 plombes à chercher une recette plus ou moins valable sur le net.

Les recettes seront juste en français, ... oui je ne sais pas les termes anglophones *of cooking*.

## Convention d'écriture

### Arborescence

Les recettes sont rangées suivants l'arborescence suivantes :

    -> Pays (ou Autre)
      -> Boisson
      -> Apéro
      -> Entrée
      -> Plat
      -> Dessert

> Un jour l'arbo sera peut être plus complexe ...

### Recette

Voici une base :

      # Titre

      Petite description

      ## Ingrédients (Pour .......)

       - Ingrédient 1,
       - Ingrédient 2.

      ## Outils

       - Outil 1,
       - Outil 2.

      ## Étapes
      
       1. Étape 1 (Préciser si variante),
       2. Étape 2.
       3. Étape 3.
       4. Étape 4.

      ## Variantes (Si existante)

      ### Ingrédients (Pour .......)

       - Ingrédient 1,
       - Ingrédient 2.

      ### Outils (Ou pas si identique)

       - Outil 1,
       - Outil 2.

      ### Étapes
      
       1. Étape variée 1,

       ...

       3. Étape variée 3.

      ## Liens utiles

       - [Nom du lien 1](http)
       - [Nom du lien 2](http)

