# Pancake

## Pâte pour 3 4 personnes (moyennement gourmandes)

 - 75g de farine T55,
 - 100mL d'eau tiède,
 - 1/2 sachet de levure chimique,
 - 2 cuillères à soupes de sucre,
 - 1 pincée de sel
 - 1 oeuf

## Outils

 - Un fouet,
 - Un récipient pour mélanger,
 - Un poele ou appareil à crèpes

## Étapes

 1. Mélanger tous les ingrédients jusqu'à obtenir une pâte un peu dense et sans grumeaux.
 2. Pas spécialement besoin de laisser reposer. Prendre une petite louche pour faire des pancakes d'une dizaine de centimètres de diamètre. Faire cuire. Et retourner.

## Variantes

### Pancake à la banane

#### Ingrédients

 - 4 bananes

#### Étapes

 2. Avant de faire cuire, découper les bananes en 3 morceaux égaux (dans la largeur). Puis découper en 4 lamelles. Mettre dans la pâte et enrober. Poser dans la poèle pour la cuisson. Et retourner.

### Pancake à la poire/pomme

#### Ingrédients

 - 2 poires/pommes

#### Étapes

 2. Avant de faire cuire, les poires pour faire des ronds. Enlever le centre. Dans le fond de la poèle, faire cuire un rond de la taille des morceaux, mettre quasi au même moment le fruit puis mettre de la pâte au-dessus. Et retourner.
