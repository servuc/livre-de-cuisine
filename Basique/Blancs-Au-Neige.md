# Blancs au neige

## Ingrédient pour des blancs au neige

 - Blancs d'oeufs (si possible fermier),
 - Eau,
 - Sel.

## Outils

 - Fouet (électrique),
 - Récipient grande contenance,
 - Casserole.

## Étapes

 1. Faire chauffer un peu d'eau dans une casserole jusqu'à ébullition et stopper,
 2. Dans le récipient mettre une pincée de sel et les blancs d'oeufs,
 3. Faire monter les blancs. Une fois qu'ils sont bien montés et fermes, avec votre main ajouté en même temps que vous mélanger doucement les blancs, quelques gouttes d'eau préalablement chauffée. Pour 2 3 oeufs, je trempe le bout de mes doigts pour les égoute au dessus des blancs à 4 5 reprises. Ainsi, les blancs resteront plus ferme.