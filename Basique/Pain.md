# Pain

## Ingrédient pour un pain de 0.5kg

 - 0.5kg de farine T100,
 - Un sachet de levure boulangère : Pour faire lever,
 - 200mL d'eau tiède (chaude du robinet, mais pas brulante),
 - Un cueillère à café de sel.

## Outils

 - Mains pour pétrire
 - Moule rectangulaire
 - Four
 - Récipient grande contenance

## Étapes

 1. Dans le récipient, mettre l'eau et le sel mélanger.
 2. Mettre la farine par dessus, puis mettre la levure. La levure ne doit pas être dans l'eau.
 3. Pétrire jusque la pate soient bien mélangée (5 minutes). Si la pâte est trop collante, ajouter de la farine, même de la T55. Si elle s'éfrite totalement, remettre un poil d'eau.
 4. Laisser reposer 1 heure minimum dans le récipient sous forme de boule. Ne pas oublier de couvrir avec un torchon.
 5. Pétrire pour faire fuire les bulles d'air, environ 1 minute max. Puis faire un espèce de cylindre de la taille du moule rectangulaire. Ne pas oublier de farine si besoin, sauf si le plat est en silicon.
 6. Laisser reposer 45 minutes minimum.
 7. Faire préchauffer 5 minutes à 180°C en chaleur tournante (ou au dessus et en dessous).
 8. Si vous avez un lèche-frites (plaque creuse), mettre 200mL pour faire de la vapeur et rendre le pain moelleux. Faire cuire 31 - 34 minutes.
 9. Sortir du four.
 10. Déguster tranche par tranche.

## Variantes

### Pain à la chataigne
 
#### Ingrédient pour un pain de 0.5kg

 - Une vingtaine de chataignes.

#### Étapes

 2. En plus, faire cuire les chataignes (30 secondes au micro-onde), les mixer. Les ajouter sur le dessus de la pâte.
