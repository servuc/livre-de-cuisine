# Fluffy Pancake

Il s'agit de pancake mais léger, que l'on peut facilement faire de telle manière qu'ils soient KawaiI

**Préparation** : 10 minutes.

**Cuisson** : 25 minutes à 125°C.

## Ingrédients (Pour 12/13 pancakes)

  - 30g de sucre blanc en poudre,
  - 150g de farine,
  - 15cL de lait,
  - 2 oeufs,
  - 1 sachet de levure chimique,
  - 1 cuillère à café de fleur d'oranger (facultatif),
  - Garnitures dépendantes de la variante.

## Outils

  - 2 récipients suivant la quantité,
  - Four (avec sa grille),
  - Papier cuisson,
  - Cuillère en bois,
  - Fouet,
  - Maryse (ou lèche-cul),
  - Emporte-pièces qui passent au four.

## Étapes

  1. Préchauffer le four (sortir la grille),
  2. Séparer les blancs des jaunes dans les deux récipients,
  3. Ajouter aux jaunes le sucre, le farine, le lait, la fleur d'oranger (et la garniture), mélanger,
  4. Ajouter la levure chimique, mélanger,
  5. Faites monter les blancs ([Des blancs qui restent fermes](Basique/Blancs-Au-Neige.md)),
  6. Avec la cuillère en bois, ajouter délicatement les blancs à la préparation,
  7. Sur la grille, disposer un papier cuisson, puis faire des petits tas de pâtes (10cm de diamètre environ). Faites gaffe de ne pas mettre trop prêt du bord, sinon ça coule en dessous ;)
  8. Mettre au four pour la cuisson,
  9. Une fois la cuisson finie, vous pouvez les rapprocher du haut du four pour les faire brunir.

> **Alors pourquoi ne pas les faire à la poêle ?**

> Suite à des expériences, le coeur ne cuit pas forcément aussi bien que l'extérieur, même à feu doux, et demande un oeil attentif, bref moins pratique ;)

## Variantes

N'hésitez pas à incorporer à la pâte (étape 3) une poignée de fruits en petits morceaux, ou entier pour des framboises (même congelées). Ou bien encore du thé matcha pour mettre avec des fraises en morceaux. Ou des pépites de chocolats.
