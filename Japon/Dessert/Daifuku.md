# Daifuku (大福)

Il s'agit d'une pâte de riz gluant, mochi, enrobant un cœur. Une des variante est *la perle de coco* que l'on trouve dans tous resto asiatiques.

## Ingrédients (Pour 6/7 daifukus)

  - 100g (et un peu plus) de farine de riz gluant,
  - 25g de sucre : seulement poudre ou 50/50 avec sucre glace,
  - Eau chaude : température du robinet. Il faut environ 5cl pour la quantité ci-dessus,
  - Garnitures dépendantes de la variante.

## Outils

  - Bol/Récipient plus grand suivant la quantité,
  - Casserole,
  - Baguettes (pas des fragiles),
  - Assiettes,
  - Cuillière à soupe (facultatif),
  - Planche en bois (facultatif).

## Étapes

  1. Préparer la garniture à l'avance.
  Quelque soit la variante, il est plus simple d'être prêt !
  2. Mélanger la farine de riz gluant au sucre dans le bol à l'aide des baguettes.
  3. Ajouter l'eau vraiment petit à petit. Mélanger avec les baguettes. La pâte va coller puis commencer à faire une boule.
  Entre 2 ajouts d'eau, mélanger quelques secondes. Le résultat n'est parfois pas immédiat.
  Il faut veiller à ce que la pate soit pil poil un tout petit peu plus friable que collant, trouver l'équilibre parfait.
  Si besoin, rajoutez de la farine petit peu, par petit peu, ou de l'eau.
  4. Faire chauffer la casserole pour *cuire* les daifukus. Vous pouvez l'éteindre dès que l'eau bout. Allez maintenant à l'étape suivante sans attrendre que l'eau bout.
  5. Enrober la garniture de la pâte. Pour cela, faite une mini crêpe d'environ 8cm de diamètre assez fine, environ 4mm. Posé la garniture au centre, rabbatre et rouler dans la main pour faire une boule. Si la pâte colle au doigt, c'est qu'elle manquait de farine. Personnelement, je les pose ensuite une planche en bois ensuite en attendant.
  6. Rallumer la casserole si besoin pour faire bouillir l'eau et la laisser bouillir. Vous plonger les daifukus. Comptez jusque 100 (secondes environ).
  Utiliser vos baguettes, ou le dos d'une cuillère à soupe, sans briser les daifukus, pour les décoller du fond de la casserole. 
  7. Sortez les. Je les mets sur une planche en bois. Laisser reposer une petite minute.
  8. Rouler dans la garniture extérieure.

## Variantes

### Noix de coco / Pâte à tartiner chocolat

#### Ingrédients (Pour 6/7 daifukus)

  - Pâte à tartiner chocolat,
  - Noix de coco rapée.

#### Étapes

  1. Surgeler une journée/nuit à l'avance dans un bac à glaçons (à demi) de pate à tartiner. Le but étant de faire des petits cubes de pâte à tartiner.
  Prévoyez autant de petits cubes que de daifukus. Sachant qu'un gros glaçons représente environ 4 cubes.

  ...

  8. Rouler dans la noix de coco rapée.

## Liens utiles

 - [geek-mexicain.net - Une recette pas si mochi que ça](https://geek-mexicain.net/une-recette-pas-si-mochi-que-ca)
 - [Wikipédia](https://fr.wikipedia.org/wiki/Daifuku)